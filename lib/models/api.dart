import 'dart:convert';

import 'package:city_explorer/utils/cache_manager.dart';
import 'dart:io';

class API{
  static API get instance => _instance;
  static API _instance = API._internal();
  API._internal();
  String _base = 'https://mapping.burlington.ca/arcgisweb/rest/services/COB';

  Future<Map<String, dynamic>> art() async{
    String url = '$_base/Public_Art/MapServer/0/query?where=1%3D1&outFields=*&geometry=&geometryType=esriGeometryEnvelope&inSR=4326&spatialRel=esriSpatialRelIntersects&outSR=4326&f=json';
    File file = await CacheManager.instance.getSingleFile(url);
    String data = await file.readAsString();

    return json.decode(data);
  }

  Future<Map<String, dynamic>> attachments(int parentId) async{
    if(parentId == null){
      return Map();
    }
    String url = '$_base/Public_Art/FeatureServer/0/$parentId/attachments?f=json';
    File file = await CacheManager.instance.getSingleFile(url);
    String data = await file.readAsString();

    return json.decode(data);
  }

  Future<File> attachment(int parentId, int attachmentId) async{

    String url = '$_base/Public_Art/FeatureServer/0/$parentId/attachments/$attachmentId';
    File file = await CacheManager.instance.getSingleFile(url);
    return file;
  }

}
