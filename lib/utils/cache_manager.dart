import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart';

class CacheManager extends BaseCacheManager {
  static const key = "appCache";

  static CacheManager get instance => _instance;
  static CacheManager _instance = CacheManager._internal();

  CacheManager._internal(): super(key,
            maxAgeCacheObject: Duration(days: 7),
            maxNrOfCacheObjects: 20,
            fileFetcher: _customHttpGetter);

  Future<String> getFilePath() async {
    var directory = await getTemporaryDirectory();
    return directory.path+'/'+key;
  }

  static Future<FileFetcherResponse> _customHttpGetter(String url,
      {Map<String, String> headers}) async {
    // Do things with headers, the url or whatever.
    return HttpFileFetcherResponse(await http.get(url, headers: headers));
  }
}
