import 'dart:io';

import 'package:built_value/json_object.dart';
import 'package:city_explorer/models/api.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home'),
      ),
      body: FutureBuilder<Map<String, dynamic>>(
        future: API.instance.art(),
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return Text(
              '${snapshot.error}',
              style:
                  Theme.of(context).textTheme.body1.copyWith(color: Colors.red),
            );
          }
          if (!snapshot.hasData) {
            return CircularProgressIndicator();
          }

          final map = snapshot.data;
          return ListView(
            shrinkWrap: true,
            children: <Widget>[
              for (var feature in map['features'])
                Card(
                  margin: EdgeInsets.all(16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      AspectRatio(
                        aspectRatio: 2.0,
                        child: Container(
                          color: Colors.grey,
                          child: FutureBuilder(
                              future: API.instance.attachments(
                                  feature['attributes']['OBJECTID']),
                              builder: (context, snapshot) {
                                if (snapshot.hasError) {
                                  return Text(
                                    '${snapshot.error}',
                                    style: Theme.of(context)
                                        .textTheme
                                        .body1
                                        .copyWith(color: Colors.red),
                                  );
                                }
                                if (!snapshot.hasData) {
                                  return CircularProgressIndicator();
                                }
                                List<dynamic> items =
                                    snapshot.data['attachmentInfos'];
                                return PageView.builder(
                                    itemCount: items.length,
                                    itemBuilder: (context, index) =>
                                        FutureBuilder<File>(
                                            future: API.instance.attachment(
                                                feature['attributes']
                                                    ['OBJECTID'],
                                                items[index]['id']),
                                            builder: (context, snapshot) {
                                              if (!snapshot.hasData) {
                                                return Text('na');
                                              }
                                              return Image.file(
                                                snapshot.data,
                                                fit: BoxFit.cover,
                                              );
                                            }));
                              }),
                        ),
                      ),

                      Padding(
                        padding: const EdgeInsets.symmetric(
                            vertical: 4.0, horizontal: 16),
                        child: Text(
                          '${feature['attributes']['NAME']}',
                          style: Theme.of(context).textTheme.title,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            vertical: 4.0, horizontal: 16),
                        child: Text(
                          '${feature['attributes']['ADDRESS']}',
                          style: Theme.of(context).textTheme.caption,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            vertical: 4.0, horizontal: 16),
                        child: Text(
                          '${feature['attributes']['ARTIST']}',
                          style: Theme.of(context).textTheme.subhead,
                        ),
                      ),

                      Padding(
                        padding: const EdgeInsets.symmetric(
                            vertical: 8.0, horizontal: 16),
                        child: Text(
                            '${feature['attributes']['DESCRIPTION_TOUR']}'),
                      ),
//                              Divider(),
//                              Text('$feature'),
                    ],
                  ),
                ),
            ],
          );
        },
      ),
    );
  }
}
